package com.ruoyi.common.utils.stream;

import org.apache.commons.exec.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * 推流相关工具类
 */
public class StreamUtils {

    private static final Logger log = LoggerFactory.getLogger(StreamUtils.class);

    private static Map<Long,Thread> threadPool = new HashMap<>();

    public static void main(String[] args) {
        String src = "rtmp://ns8.indexforce.com/home/mystream";
        String dest = "rtmps://dc5-1.rtmp.t.me/s/1970943417:doAo2Ia2i8kdhikxb-vcNg";
        pushStream(src,dest);
    }

    public static void stopStream(Long id){
        Thread thread = threadPool.get(id);
        log.info("拉流转推线程池,size-{}",threadPool.size());
        if(thread!=null&& thread.isAlive()){
            thread.interrupt();
        }
        threadPool.remove(id);
    }

    public static boolean checkStream(Long id){
        Thread thread = threadPool.get(id);
        log.info("拉流转推线程池,池数量: {}",threadPool.size());
        if(thread!=null&& thread.isAlive()){
            log.info("线程运行中,{}",id);
            return true;
        }
        threadPool.remove(id);
        log.info("移除失效线程,{}",id);
        return false;
    }

    public static Long pushStream(String src,String dest){
        String command = "ffmpeg -i %s -f lavfi -t 10 -i anullsrc=r=44100:cl=stereo -c:v copy -c:a aac -strict experimental -f flv %s";
        command = String.format(command,src,dest);
        log.info("拉流转推开始:\n{}\n",command);
        //接收正常结果流
        ByteArrayOutputStream susStream = new ByteArrayOutputStream();
        //接收异常结果流
        ByteArrayOutputStream errStream = new ByteArrayOutputStream();
        CommandLine commandLine = CommandLine.parse(command);
        DefaultExecutor exec = new DefaultExecutor();

        PumpStreamHandler streamHandler = new PumpStreamHandler(susStream, errStream);
        exec.setStreamHandler(streamHandler);
        String finalCommand = command;

        ExecuteResultHandler erh = new ExecuteResultHandler() {
            @Override
            public void onProcessComplete(int exitValue) {
                try {
                    String suc = susStream.toString("GBK");
                    log.info("异步执行完成,{}",suc);
                    threadPool.remove(Thread.currentThread().getId());
                    Thread.currentThread().interrupt();
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
            }
            @Override
            public void onProcessFailed(ExecuteException e) {
                try {
                    String err = errStream.toString("GBK");
                    log.error("异步执行出错,\n{}\n,{}", finalCommand,err);
                    threadPool.remove(Thread.currentThread().getId());
                    Thread.currentThread().interrupt();
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
            }
        };

        Thread t = new Thread(()->{
            try {
                exec.execute(commandLine, erh);
                while (true){
                    Thread.sleep(1000*60*10);
                }
            }catch (Exception e){
                log.error("拉流转推异常退出，{}",e);
            }
        });
        t.start();

        threadPool.put(t.getId(),t);
        log.info("任务分发完成，源：{}，目标：{}",src,dest);
        return t.getId();
    }

}
