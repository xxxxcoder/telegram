/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50743
 Source Host           : localhost:3306
 Source Schema         : ruoyi

 Target Server Type    : MySQL
 Target Server Version : 50743
 File Encoding         : 65001

 Date: 22/10/2023 09:16:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tl_chat
-- ----------------------------
DROP TABLE IF EXISTS `tl_chat`;
CREATE TABLE `tl_chat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '对话名称',
  `chat_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电报对话id',
  `stream_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电报播流地址',
  `stream_src` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '推流源地址',
  `league_id` int(20) NULL DEFAULT NULL COMMENT '联赛id',
  `league_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联赛名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '电报-会话表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_chat
-- ----------------------------

-- ----------------------------
-- Table structure for tl_game
-- ----------------------------
DROP TABLE IF EXISTS `tl_game`;
CREATE TABLE `tl_game`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `league_id` int(20) NULL DEFAULT NULL,
  `league_zh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `league_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `league_en_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '0:足球；1：篮球',
  `team_home_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主队名称',
  `team_guest_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客队名称',
  `team_home_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主队logo',
  `team_guest_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客队logo',
  `begin_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `begin_date` datetime NULL DEFAULT NULL COMMENT '开始日期',
  `team_home_score` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主队得分',
  `team_guest_score` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客队得分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '赛事赛程' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_game
-- ----------------------------

-- ----------------------------
-- Table structure for tl_game_stream
-- ----------------------------
DROP TABLE IF EXISTS `tl_game_stream`;
CREATE TABLE `tl_game_stream`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '线路url',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '线路名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '赛事线路' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_game_stream
-- ----------------------------

-- ----------------------------
-- Table structure for tl_league
-- ----------------------------
DROP TABLE IF EXISTS `tl_league`;
CREATE TABLE `tl_league`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `league_zh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `league_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `league_en_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '0:足球；1：篮球',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '联赛' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_league
-- ----------------------------

-- ----------------------------
-- Table structure for tl_team
-- ----------------------------
DROP TABLE IF EXISTS `tl_team`;
CREATE TABLE `tl_team`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `team_zh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `team_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `team_en_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '0:足球；1：篮球',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '球队' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_team
-- ----------------------------

-- ----------------------------
-- Table structure for tl_team_player
-- ----------------------------
DROP TABLE IF EXISTS `tl_team_player`;
CREATE TABLE `tl_team_player`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `team_id` int(20) NULL DEFAULT NULL COMMENT '球队id',
  `player_zh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `player_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `player_en_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '0:足球；1：篮球',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '球员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tl_team_player
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
