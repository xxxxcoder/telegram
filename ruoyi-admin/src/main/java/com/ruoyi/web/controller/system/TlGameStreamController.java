package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TlGameStream;
import com.ruoyi.system.service.ITlGameStreamService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 赛事线路Controller
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Controller
@RequestMapping("/system/stream")
public class TlGameStreamController extends BaseController
{
    private String prefix = "system/stream";

    @Autowired
    private ITlGameStreamService tlGameStreamService;

    @RequiresPermissions("system:stream:view")
    @GetMapping()
    public String stream()
    {
        return prefix + "/stream";
    }

    /**
     * 查询赛事线路列表
     */
    @RequiresPermissions("system:stream:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TlGameStream tlGameStream)
    {
        startPage();
        List<TlGameStream> list = tlGameStreamService.selectTlGameStreamList(tlGameStream);
        return getDataTable(list);
    }

    /**
     * 导出赛事线路列表
     */
    @RequiresPermissions("system:stream:export")
    @Log(title = "赛事线路", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TlGameStream tlGameStream)
    {
        List<TlGameStream> list = tlGameStreamService.selectTlGameStreamList(tlGameStream);
        ExcelUtil<TlGameStream> util = new ExcelUtil<TlGameStream>(TlGameStream.class);
        return util.exportExcel(list, "赛事线路数据");
    }

    /**
     * 新增赛事线路
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存赛事线路
     */
    @RequiresPermissions("system:stream:add")
    @Log(title = "赛事线路", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TlGameStream tlGameStream)
    {
        return toAjax(tlGameStreamService.insertTlGameStream(tlGameStream));
    }

    /**
     * 修改赛事线路
     */
    @RequiresPermissions("system:stream:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TlGameStream tlGameStream = tlGameStreamService.selectTlGameStreamById(id);
        mmap.put("tlGameStream", tlGameStream);
        return prefix + "/edit";
    }

    /**
     * 修改保存赛事线路
     */
    @RequiresPermissions("system:stream:edit")
    @Log(title = "赛事线路", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TlGameStream tlGameStream)
    {
        return toAjax(tlGameStreamService.updateTlGameStream(tlGameStream));
    }

    /**
     * 删除赛事线路
     */
    @RequiresPermissions("system:stream:remove")
    @Log(title = "赛事线路", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tlGameStreamService.deleteTlGameStreamByIds(ids));
    }
}
