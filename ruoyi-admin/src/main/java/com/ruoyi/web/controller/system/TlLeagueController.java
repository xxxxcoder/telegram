package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TlLeague;
import com.ruoyi.system.service.ITlLeagueService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 联赛Controller
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Controller
@RequestMapping("/system/league")
public class TlLeagueController extends BaseController
{
    private String prefix = "system/league";

    @Autowired
    private ITlLeagueService tlLeagueService;

    @RequiresPermissions("system:league:view")
    @GetMapping()
    public String league()
    {
        return prefix + "/league";
    }

    /**
     * 查询联赛列表
     */
    @RequiresPermissions("system:league:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TlLeague tlLeague)
    {
        startPage();
        List<TlLeague> list = tlLeagueService.selectTlLeagueList(tlLeague);
        return getDataTable(list);
    }

    /**
     * 导出联赛列表
     */
    @RequiresPermissions("system:league:export")
    @Log(title = "联赛", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TlLeague tlLeague)
    {
        List<TlLeague> list = tlLeagueService.selectTlLeagueList(tlLeague);
        ExcelUtil<TlLeague> util = new ExcelUtil<TlLeague>(TlLeague.class);
        return util.exportExcel(list, "联赛数据");
    }

    /**
     * 新增联赛
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存联赛
     */
    @RequiresPermissions("system:league:add")
    @Log(title = "联赛", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TlLeague tlLeague)
    {
        return toAjax(tlLeagueService.insertTlLeague(tlLeague));
    }

    /**
     * 修改联赛
     */
    @RequiresPermissions("system:league:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TlLeague tlLeague = tlLeagueService.selectTlLeagueById(id);
        mmap.put("tlLeague", tlLeague);
        return prefix + "/edit";
    }

    /**
     * 修改保存联赛
     */
    @RequiresPermissions("system:league:edit")
    @Log(title = "联赛", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TlLeague tlLeague)
    {
        return toAjax(tlLeagueService.updateTlLeague(tlLeague));
    }

    /**
     * 删除联赛
     */
    @RequiresPermissions("system:league:remove")
    @Log(title = "联赛", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tlLeagueService.deleteTlLeagueByIds(ids));
    }
}
