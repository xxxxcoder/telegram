package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.utils.stream.StreamUtils;
import com.ruoyi.system.domain.TlGame;
import com.ruoyi.system.domain.TlGameStream;
import com.ruoyi.system.domain.TlLeague;
import com.ruoyi.system.service.ITlGameService;
import com.ruoyi.system.service.ITlGameStreamService;
import com.ruoyi.system.service.ITlLeagueService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TlChat;
import com.ruoyi.system.service.ITlChatService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 电报-会话Controller
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
@Controller
@RequestMapping("/system/chat")
public class TlChatController extends BaseController
{
    private String prefix = "system/chat";

    @Autowired
    private ITlChatService tlChatService;
    @Autowired
    private ITlLeagueService leagueService;
    @Autowired
    private ITlGameService gameService;
    @Autowired
    private ITlGameStreamService streamService;

    @RequiresPermissions("system:chat:view")
    @GetMapping()
    public String chat()
    {
        return prefix + "/chat";
    }

    /**
     * 查询电报-会话列表
     */
    @RequiresPermissions("system:chat:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TlChat tlChat)
    {
        startPage();
        List<TlChat> list = tlChatService.selectTlChatList(tlChat);
        return getDataTable(list);
    }

    /**
     * 导出电报-会话列表
     */
    @RequiresPermissions("system:chat:export")
    @Log(title = "电报-会话", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TlChat tlChat)
    {
        List<TlChat> list = tlChatService.selectTlChatList(tlChat);
        ExcelUtil<TlChat> util = new ExcelUtil<TlChat>(TlChat.class);
        return util.exportExcel(list, "电报-会话数据");
    }

    /**
     * 新增电报-会话
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("league", leagueService.selectTlLeagueList(new TlLeague()));
        return prefix + "/add";
    }

    /**
     * 新增保存电报-会话
     */
    @RequiresPermissions("system:chat:add")
    @Log(title = "电报-会话", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TlChat tlChat)
    {
        return toAjax(tlChatService.insertTlChat(tlChat));
    }

    /**
     * 修改电报-会话
     */
    @RequiresPermissions("system:chat:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TlChat tlChat = tlChatService.selectTlChatById(id);
        mmap.put("tlChat", tlChat);
        mmap.put("league", leagueService.selectTlLeagueList(new TlLeague()));
        return prefix + "/edit";
    }

    /**
     * 修改保存电报-会话
     */
    @RequiresPermissions("system:chat:edit")
    @Log(title = "电报-会话", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TlChat tlChat)
    {
        return toAjax(tlChatService.updateTlChat(tlChat));
    }

    /**
     * 删除电报-会话
     */
    @RequiresPermissions("system:chat:remove")
    @Log(title = "电报-会话", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tlChatService.deleteTlChatByIds(ids));
    }

    /**
     * 启动会话推流
     * @param tlChat
     * @return
     */
    @Log(title = "启动会话推流", businessType = BusinessType.OTHER)
    @PostMapping("/pushStreamStart")
    @ResponseBody
    public AjaxResult pushStreamStart(TlChat tlChat)
    {
        TlChat chat = tlChatService.selectTlChatById(tlChat.getId());
        StreamUtils.stopStream(Long.valueOf(chat.getChatId()));
        Long tid = StreamUtils.pushStream(chat.getStreamSrc(), chat.getStreamUrl());
        chat.setChatId(String.valueOf(tid));
        tlChatService.updateTlChat(chat);
        return AjaxResult.success();
    }

    /**
     * 关闭会话推流
     * @param tlChat
     * @return
     */
    @Log(title = "关闭会话推流", businessType = BusinessType.OTHER)
    @PostMapping("/pushStreamStop")
    @ResponseBody
    public AjaxResult pushStreamStop(TlChat tlChat)
    {
        TlChat chat = tlChatService.selectTlChatById(tlChat.getId());
        StreamUtils.stopStream(Long.valueOf(chat.getChatId()));
        chat.setChatId("0");
        tlChatService.updateTlChat(chat);
        return AjaxResult.success();
    }

    @PostMapping("/updateStream")
    @ResponseBody
    public AjaxResult updateStream(TlChat tlChat)
    {
        TlChat chat = tlChatService.selectTlChatById(tlChat.getId());
        chat.setStreamUrl(tlChat.getStreamUrl());
        tlChatService.updateTlChat(chat);
        return AjaxResult.success();
    }

    @GetMapping( "/games")
    @ResponseBody
    public AjaxResult games(String leagueId)
    {
        TlGame game = new TlGame();
        game.setLeagueId(Long.valueOf(leagueId));
        List<TlGame> games = gameService.selectTlGameList(game);
        return AjaxResult.success(games);
    }

    @GetMapping( "/streams")
    @ResponseBody
    public AjaxResult streams(String gameId)
    {
        TlGameStream stream = new TlGameStream();
        stream.setGameId(Long.valueOf(gameId));
        List<TlGameStream> streams =streamService.selectTlGameStreamList(stream);
        return AjaxResult.success(streams);
    }
}
