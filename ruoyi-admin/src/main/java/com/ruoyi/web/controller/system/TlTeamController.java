package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TlTeam;
import com.ruoyi.system.service.ITlTeamService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 球队Controller
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Controller
@RequestMapping("/system/team")
public class TlTeamController extends BaseController
{
    private String prefix = "system/team";

    @Autowired
    private ITlTeamService tlTeamService;

    @RequiresPermissions("system:team:view")
    @GetMapping()
    public String team()
    {
        return prefix + "/team";
    }

    /**
     * 查询球队列表
     */
    @RequiresPermissions("system:team:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TlTeam tlTeam)
    {
        startPage();
        List<TlTeam> list = tlTeamService.selectTlTeamList(tlTeam);
        return getDataTable(list);
    }

    /**
     * 导出球队列表
     */
    @RequiresPermissions("system:team:export")
    @Log(title = "球队", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TlTeam tlTeam)
    {
        List<TlTeam> list = tlTeamService.selectTlTeamList(tlTeam);
        ExcelUtil<TlTeam> util = new ExcelUtil<TlTeam>(TlTeam.class);
        return util.exportExcel(list, "球队数据");
    }

    /**
     * 新增球队
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存球队
     */
    @RequiresPermissions("system:team:add")
    @Log(title = "球队", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TlTeam tlTeam)
    {
        return toAjax(tlTeamService.insertTlTeam(tlTeam));
    }

    /**
     * 修改球队
     */
    @RequiresPermissions("system:team:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TlTeam tlTeam = tlTeamService.selectTlTeamById(id);
        mmap.put("tlTeam", tlTeam);
        return prefix + "/edit";
    }

    /**
     * 修改保存球队
     */
    @RequiresPermissions("system:team:edit")
    @Log(title = "球队", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TlTeam tlTeam)
    {
        return toAjax(tlTeamService.updateTlTeam(tlTeam));
    }

    /**
     * 删除球队
     */
    @RequiresPermissions("system:team:remove")
    @Log(title = "球队", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tlTeamService.deleteTlTeamByIds(ids));
    }
}
