package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TlGame;
import com.ruoyi.system.service.ITlGameService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 赛事赛程Controller
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Controller
@RequestMapping("/system/game")
public class TlGameController extends BaseController
{
    private String prefix = "system/game";

    @Autowired
    private ITlGameService tlGameService;

    @RequiresPermissions("system:game:view")
    @GetMapping()
    public String game()
    {
        return prefix + "/game";
    }

    /**
     * 查询赛事赛程列表
     */
    @RequiresPermissions("system:game:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TlGame tlGame)
    {
        startPage();
        List<TlGame> list = tlGameService.selectTlGameList(tlGame);
        return getDataTable(list);
    }

    /**
     * 导出赛事赛程列表
     */
    @RequiresPermissions("system:game:export")
    @Log(title = "赛事赛程", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TlGame tlGame)
    {
        List<TlGame> list = tlGameService.selectTlGameList(tlGame);
        ExcelUtil<TlGame> util = new ExcelUtil<TlGame>(TlGame.class);
        return util.exportExcel(list, "赛事赛程数据");
    }

    /**
     * 新增赛事赛程
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存赛事赛程
     */
    @RequiresPermissions("system:game:add")
    @Log(title = "赛事赛程", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TlGame tlGame)
    {
        return toAjax(tlGameService.insertTlGame(tlGame));
    }

    /**
     * 修改赛事赛程
     */
    @RequiresPermissions("system:game:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TlGame tlGame = tlGameService.selectTlGameById(id);
        mmap.put("tlGame", tlGame);
        return prefix + "/edit";
    }

    /**
     * 修改保存赛事赛程
     */
    @RequiresPermissions("system:game:edit")
    @Log(title = "赛事赛程", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TlGame tlGame)
    {
        return toAjax(tlGameService.updateTlGame(tlGame));
    }

    /**
     * 删除赛事赛程
     */
    @RequiresPermissions("system:game:remove")
    @Log(title = "赛事赛程", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tlGameService.deleteTlGameByIds(ids));
    }
}
