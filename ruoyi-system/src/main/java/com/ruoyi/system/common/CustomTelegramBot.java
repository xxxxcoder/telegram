package com.ruoyi.system.common;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.*;
import com.pengrad.telegrambot.model.botcommandscope.BotCommandScopeAllChatAdministrators;
import com.pengrad.telegrambot.model.botcommandscope.BotCommandScopeDefault;
import com.pengrad.telegrambot.model.botcommandscope.BotCommandsScopeChat;
import com.pengrad.telegrambot.model.botcommandscope.BotCommandsScopeChatMember;
import com.pengrad.telegrambot.model.request.*;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SetChatMenuButton;
import com.pengrad.telegrambot.request.SetMyCommands;
import com.pengrad.telegrambot.response.BaseResponse;
import com.pengrad.telegrambot.response.SendResponse;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.TlGame;
import com.ruoyi.system.domain.TlLeague;
import com.ruoyi.system.mapper.TlGameMapper;
import com.ruoyi.system.mapper.TlLeagueMapper;
import com.ruoyi.system.service.impl.SysConfigServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CustomTelegramBot {

    private static final Logger log = LoggerFactory.getLogger(CustomTelegramBot.class);
    private static TelegramBot bot;

    /**
     * 获取InlineKeyboardMarkup联赛列表
     * @return
     */
    public static InlineKeyboardMarkup getInLeagues() {
        InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup();
        List<TlLeague> leagues = SpringUtils.getBean(TlLeagueMapper.class).selectTlLeagueList(new TlLeague());
        for (TlLeague league : leagues) {
            inlineKeyboard.addRow( new InlineKeyboardButton(league.getLeagueZhName()));
        }
        return inlineKeyboard;
    }

    /**
     * 获取Keyboard联赛列表
     * @return
     */
    public static Keyboard getLeagues() {
//        KeyboardButton[][] buttons = new KeyboardButton[5][10];

        List<TlLeague> leagues = SpringUtils.getBean(TlLeagueMapper.class).selectTlLeagueList(new TlLeague());
        KeyboardButton[] buttons = new KeyboardButton[leagues.size()];
        for (TlLeague league : leagues) {
//            List<TlGame> games = SpringUtils.getBean(TlGameMapper.class).selectTlGameList(new TlGame());
//            for (TlGame game : games) {
//                buttons[leagues.indexOf(league)][games.indexOf(game)] = new KeyboardButton(game.getTeamHomeName() +" vs "+ game.getTeamGuestName());
//            }
            buttons[leagues.indexOf(league)] = new KeyboardButton(league.getLeagueZhName());
        }
        Keyboard keyboard = new ReplyKeyboardMarkup(buttons);

        return keyboard;
    }

    /**
     * 获取比赛列表
     * @return
     */
    public static InlineKeyboardMarkup getGames() {
        InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup();
        List<TlGame> games = SpringUtils.getBean(TlGameMapper.class).selectTlGameList(new TlGame());
        for (TlGame game : games) {
            inlineKeyboard.addRow( new InlineKeyboardButton(game.getTeamHomeName() +" vs "+ game.getTeamGuestName()));
        }
        return inlineKeyboard;
    }

    public static String textInline(String input){
        StringBuilder reply = new StringBuilder();
        if(input.contains("start")){
            List<TlLeague> leagues = SpringUtils.getBean(TlLeagueMapper.class).selectTlLeagueList(new TlLeague());
            reply.append("联赛清单：").append("\r\n");
            for (TlLeague league : leagues) {
               reply.append("-/league").append(league.getId()).append("-").append(league.getLeagueZhName()).append("\r\n");
            }
        }else if(input.contains("league")){

            List<TlGame> games = SpringUtils.getBean(TlGameMapper.class).selectTlGameList(new TlGame());
            reply.append("赛事清单：").append("\r\n");
            for (TlGame game : games) {
                reply.append("-/game").append(game.getId()).append("-")
                        .append(game.getBeginTime()).append("\r\n")
                        .append(game.getTeamHomeName() +" vs "+ game.getTeamGuestName())
                        .append("\r\n");
            }
        }else {
            reply.append("/start").append("\r\n");
            reply.append("/league").append("\r\n");
            reply.append("/game").append("\r\n");
        }

        return reply.toString();
    }

    @Deprecated
    public static InlineKeyboardMarkup getStreams() {
        Keyboard keyboard = new ReplyKeyboardMarkup(
                new KeyboardButton[]{
                        new KeyboardButton("text"),
                        new KeyboardButton("contact").requestContact(true),
                        new KeyboardButton("location").requestLocation(true)
                }
        );

        InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup(
                new InlineKeyboardButton[]{
                        new InlineKeyboardButton("url").url("www.google.com"),
                        new InlineKeyboardButton("callback_data").callbackData("callback_data"),
                        new InlineKeyboardButton("Switch!").switchInlineQuery("switch_inline_query")
                });


        return inlineKeyboard;
    }

    public static TelegramBot getInstance() {
        if(bot==null){
            SysConfigServiceImpl bean = SpringUtils.getBean(SysConfigServiceImpl.class);
            String botToken = bean.selectConfigByKey("tl.bot.token");
            bot = new TelegramBot(botToken);

            BotCommand[] commands = new BotCommand[]{
                    new BotCommand("/start", "开始使用"),
                    new BotCommand("/league", "联赛名录"),
                    new BotCommand("/game", "赛事赛程"),
            };
            SetMyCommands cmds = new SetMyCommands(commands);
            cmds.languageCode("en");
            cmds.scope(new BotCommandScopeDefault());
            bot.execute(cmds);

            bot.setUpdatesListener(updates -> {
                for (Update update : updates) {
                    if(update==null || update.message()==null || update.message().text()==null){
                        break;
                    }
                    log.info(update.message().text());
                    long chatId = update.message().chat().id();

                    bot.execute(new SetChatMenuButton().chatId(chatId).menuButton(new MenuButtonCommands()));

                    SendMessage message = new SendMessage(chatId, textInline(update.message().text()));
                    message .parseMode(ParseMode.HTML)
                            .disableWebPagePreview(true)
                            .replyMarkup(new ReplyKeyboardRemove());
//                            .replyMarkup(getLeagues());
                    SendResponse response = bot.execute(message);
                }
                return UpdatesListener.CONFIRMED_UPDATES_ALL;
            }, e -> {
                if (e.response() != null) {
                    // got bad response from telegram
                    e.response().errorCode();
                    e.response().description();
                } else {
                    // probably network error
                    e.printStackTrace();
                }
            });
        }

        return bot;
    }
}
