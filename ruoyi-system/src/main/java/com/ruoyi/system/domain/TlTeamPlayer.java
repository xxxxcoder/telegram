package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 球员对象 tl_team_player
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public class TlTeamPlayer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 球队id */
    @Excel(name = "球队id")
    private Long teamId;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String playerZhName;

    /** 图标 */
    @Excel(name = "图标")
    private String playerLogo;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String playerEnName;

    /** 0:足球；1：篮球 */
    @Excel(name = "0:足球；1：篮球")
    private Long type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTeamId(Long teamId) 
    {
        this.teamId = teamId;
    }

    public Long getTeamId() 
    {
        return teamId;
    }
    public void setPlayerZhName(String playerZhName) 
    {
        this.playerZhName = playerZhName;
    }

    public String getPlayerZhName() 
    {
        return playerZhName;
    }
    public void setPlayerLogo(String playerLogo) 
    {
        this.playerLogo = playerLogo;
    }

    public String getPlayerLogo() 
    {
        return playerLogo;
    }
    public void setPlayerEnName(String playerEnName) 
    {
        this.playerEnName = playerEnName;
    }

    public String getPlayerEnName() 
    {
        return playerEnName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("teamId", getTeamId())
            .append("playerZhName", getPlayerZhName())
            .append("playerLogo", getPlayerLogo())
            .append("playerEnName", getPlayerEnName())
            .append("type", getType())
            .toString();
    }
}
