package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 球队对象 tl_team
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public class TlTeam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String teamZhName;

    /** 图标 */
    @Excel(name = "图标")
    private String teamLogo;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String teamEnName;

    /** 0:足球；1：篮球 */
    @Excel(name = "0:足球；1：篮球")
    private Long type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTeamZhName(String teamZhName) 
    {
        this.teamZhName = teamZhName;
    }

    public String getTeamZhName() 
    {
        return teamZhName;
    }
    public void setTeamLogo(String teamLogo) 
    {
        this.teamLogo = teamLogo;
    }

    public String getTeamLogo() 
    {
        return teamLogo;
    }
    public void setTeamEnName(String teamEnName) 
    {
        this.teamEnName = teamEnName;
    }

    public String getTeamEnName() 
    {
        return teamEnName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("teamZhName", getTeamZhName())
            .append("teamLogo", getTeamLogo())
            .append("teamEnName", getTeamEnName())
            .append("type", getType())
            .toString();
    }
}
