package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 联赛对象 tl_league
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public class TlLeague extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String leagueZhName;

    /** 图标 */
    @Excel(name = "图标")
    private String leagueLogo;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String leagueEnName;

    /** 0:足球；1：篮球 */
    @Excel(name = "0:足球；1：篮球")
    private Long type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLeagueZhName(String leagueZhName) 
    {
        this.leagueZhName = leagueZhName;
    }

    public String getLeagueZhName() 
    {
        return leagueZhName;
    }
    public void setLeagueLogo(String leagueLogo) 
    {
        this.leagueLogo = leagueLogo;
    }

    public String getLeagueLogo() 
    {
        return leagueLogo;
    }
    public void setLeagueEnName(String leagueEnName) 
    {
        this.leagueEnName = leagueEnName;
    }

    public String getLeagueEnName() 
    {
        return leagueEnName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("leagueZhName", getLeagueZhName())
            .append("leagueLogo", getLeagueLogo())
            .append("leagueEnName", getLeagueEnName())
            .append("type", getType())
            .toString();
    }
}
