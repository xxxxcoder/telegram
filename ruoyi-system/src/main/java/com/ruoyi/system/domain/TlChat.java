package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会话群组对象 tl_chat
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public class TlChat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 对话名称 */
    @Excel(name = "对话名称")
    private String chatName;

    /** 电报对话id */
    @Excel(name = "电报对话id")
    private String chatId;

    /** 电报播流地址 */
    @Excel(name = "电报播流地址")
    private String streamUrl;

    /** 推流源地址 */
    @Excel(name = "推流源地址")
    private String streamSrc;

    /** 联赛id */
    @Excel(name = "联赛id")
    private Long leagueId;

    /** 联赛名称 */
    @Excel(name = "联赛名称")
    private String leagueName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChatName(String chatName) 
    {
        this.chatName = chatName;
    }

    public String getChatName() 
    {
        return chatName;
    }
    public void setChatId(String chatId) 
    {
        this.chatId = chatId;
    }

    public String getChatId() 
    {
        return chatId;
    }
    public void setStreamUrl(String streamUrl) 
    {
        this.streamUrl = streamUrl;
    }

    public String getStreamUrl() 
    {
        return streamUrl;
    }
    public void setStreamSrc(String streamSrc) 
    {
        this.streamSrc = streamSrc;
    }

    public String getStreamSrc() 
    {
        return streamSrc;
    }
    public void setLeagueId(Long leagueId) 
    {
        this.leagueId = leagueId;
    }

    public Long getLeagueId() 
    {
        return leagueId;
    }
    public void setLeagueName(String leagueName) 
    {
        this.leagueName = leagueName;
    }

    public String getLeagueName() 
    {
        return leagueName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chatName", getChatName())
            .append("chatId", getChatId())
            .append("streamUrl", getStreamUrl())
            .append("streamSrc", getStreamSrc())
            .append("leagueId", getLeagueId())
            .append("leagueName", getLeagueName())
            .toString();
    }
}
