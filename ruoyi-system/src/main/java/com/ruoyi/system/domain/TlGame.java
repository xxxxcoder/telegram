package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 赛事赛程对象 tl_game
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public class TlGame extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @Excel(name = "")
    private Long leagueId;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String leagueZhName;

    /** 图标 */
    @Excel(name = "图标")
    private String leagueLogo;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String leagueEnName;

    /** 0:足球；1：篮球 */
    @Excel(name = "0:足球；1：篮球")
    private Long type;

    /** 主队名称 */
    @Excel(name = "主队名称")
    private String teamHomeName;

    /** 客队名称 */
    @Excel(name = "客队名称")
    private String teamGuestName;

    /** 主队logo */
    @Excel(name = "主队logo")
    private String teamHomeLogo;

    /** 客队logo */
    @Excel(name = "客队logo")
    private String teamGuestLogo;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginDate;

    /** 主队得分 */
    @Excel(name = "主队得分")
    private String teamHomeScore;

    /** 客队得分 */
    @Excel(name = "客队得分")
    private String teamGuestScore;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLeagueId(Long leagueId) 
    {
        this.leagueId = leagueId;
    }

    public Long getLeagueId() 
    {
        return leagueId;
    }
    public void setLeagueZhName(String leagueZhName) 
    {
        this.leagueZhName = leagueZhName;
    }

    public String getLeagueZhName() 
    {
        return leagueZhName;
    }
    public void setLeagueLogo(String leagueLogo) 
    {
        this.leagueLogo = leagueLogo;
    }

    public String getLeagueLogo() 
    {
        return leagueLogo;
    }
    public void setLeagueEnName(String leagueEnName) 
    {
        this.leagueEnName = leagueEnName;
    }

    public String getLeagueEnName() 
    {
        return leagueEnName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setTeamHomeName(String teamHomeName) 
    {
        this.teamHomeName = teamHomeName;
    }

    public String getTeamHomeName() 
    {
        return teamHomeName;
    }
    public void setTeamGuestName(String teamGuestName) 
    {
        this.teamGuestName = teamGuestName;
    }

    public String getTeamGuestName() 
    {
        return teamGuestName;
    }
    public void setTeamHomeLogo(String teamHomeLogo) 
    {
        this.teamHomeLogo = teamHomeLogo;
    }

    public String getTeamHomeLogo() 
    {
        return teamHomeLogo;
    }
    public void setTeamGuestLogo(String teamGuestLogo) 
    {
        this.teamGuestLogo = teamGuestLogo;
    }

    public String getTeamGuestLogo() 
    {
        return teamGuestLogo;
    }
    public void setBeginTime(Date beginTime) 
    {
        this.beginTime = beginTime;
    }

    public Date getBeginTime() 
    {
        return beginTime;
    }
    public void setBeginDate(Date beginDate) 
    {
        this.beginDate = beginDate;
    }

    public Date getBeginDate() 
    {
        return beginDate;
    }
    public void setTeamHomeScore(String teamHomeScore) 
    {
        this.teamHomeScore = teamHomeScore;
    }

    public String getTeamHomeScore() 
    {
        return teamHomeScore;
    }
    public void setTeamGuestScore(String teamGuestScore) 
    {
        this.teamGuestScore = teamGuestScore;
    }

    public String getTeamGuestScore() 
    {
        return teamGuestScore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("leagueId", getLeagueId())
            .append("leagueZhName", getLeagueZhName())
            .append("leagueLogo", getLeagueLogo())
            .append("leagueEnName", getLeagueEnName())
            .append("type", getType())
            .append("teamHomeName", getTeamHomeName())
            .append("teamGuestName", getTeamGuestName())
            .append("teamHomeLogo", getTeamHomeLogo())
            .append("teamGuestLogo", getTeamGuestLogo())
            .append("beginTime", getBeginTime())
            .append("beginDate", getBeginDate())
            .append("teamHomeScore", getTeamHomeScore())
            .append("teamGuestScore", getTeamGuestScore())
            .toString();
    }
}
