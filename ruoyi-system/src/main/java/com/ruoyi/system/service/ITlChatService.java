package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TlChat;

/**
 * 电报-会话Service接口
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
public interface ITlChatService 
{
    /**
     * 查询电报-会话
     * 
     * @param id 电报-会话主键
     * @return 电报-会话
     */
    public TlChat selectTlChatById(Long id);

    /**
     * 查询电报-会话列表
     * 
     * @param tlChat 电报-会话
     * @return 电报-会话集合
     */
    public List<TlChat> selectTlChatList(TlChat tlChat);

    /**
     * 新增电报-会话
     * 
     * @param tlChat 电报-会话
     * @return 结果
     */
    public int insertTlChat(TlChat tlChat);

    /**
     * 修改电报-会话
     * 
     * @param tlChat 电报-会话
     * @return 结果
     */
    public int updateTlChat(TlChat tlChat);

    /**
     * 批量删除电报-会话
     * 
     * @param ids 需要删除的电报-会话主键集合
     * @return 结果
     */
    public int deleteTlChatByIds(String ids);

    /**
     * 删除电报-会话信息
     * 
     * @param id 电报-会话主键
     * @return 结果
     */
    public int deleteTlChatById(Long id);

    /**
     * 自动推流
     */
    public void autoPushStream();
}
