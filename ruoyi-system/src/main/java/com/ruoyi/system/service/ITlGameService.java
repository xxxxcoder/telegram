package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TlGame;

/**
 * 赛事赛程Service接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface ITlGameService 
{
    /**
     * 查询赛事赛程
     * 
     * @param id 赛事赛程主键
     * @return 赛事赛程
     */
    public TlGame selectTlGameById(Long id);

    /**
     * 查询赛事赛程列表
     * 
     * @param tlGame 赛事赛程
     * @return 赛事赛程集合
     */
    public List<TlGame> selectTlGameList(TlGame tlGame);

    /**
     * 新增赛事赛程
     * 
     * @param tlGame 赛事赛程
     * @return 结果
     */
    public int insertTlGame(TlGame tlGame);

    /**
     * 修改赛事赛程
     * 
     * @param tlGame 赛事赛程
     * @return 结果
     */
    public int updateTlGame(TlGame tlGame);

    /**
     * 批量删除赛事赛程
     * 
     * @param ids 需要删除的赛事赛程主键集合
     * @return 结果
     */
    public int deleteTlGameByIds(String ids);

    /**
     * 删除赛事赛程信息
     * 
     * @param id 赛事赛程主键
     * @return 结果
     */
    public int deleteTlGameById(Long id);
}
