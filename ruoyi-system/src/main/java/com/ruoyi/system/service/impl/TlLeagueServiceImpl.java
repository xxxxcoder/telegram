package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlLeagueMapper;
import com.ruoyi.system.domain.TlLeague;
import com.ruoyi.system.service.ITlLeagueService;
import com.ruoyi.common.core.text.Convert;

/**
 * 联赛Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Service
public class TlLeagueServiceImpl implements ITlLeagueService 
{
    @Autowired
    private TlLeagueMapper tlLeagueMapper;

    /**
     * 查询联赛
     * 
     * @param id 联赛主键
     * @return 联赛
     */
    @Override
    public TlLeague selectTlLeagueById(Long id)
    {
        return tlLeagueMapper.selectTlLeagueById(id);
    }

    /**
     * 查询联赛列表
     * 
     * @param tlLeague 联赛
     * @return 联赛
     */
    @Override
    public List<TlLeague> selectTlLeagueList(TlLeague tlLeague)
    {
        return tlLeagueMapper.selectTlLeagueList(tlLeague);
    }

    /**
     * 新增联赛
     * 
     * @param tlLeague 联赛
     * @return 结果
     */
    @Override
    public int insertTlLeague(TlLeague tlLeague)
    {
        return tlLeagueMapper.insertTlLeague(tlLeague);
    }

    /**
     * 修改联赛
     * 
     * @param tlLeague 联赛
     * @return 结果
     */
    @Override
    public int updateTlLeague(TlLeague tlLeague)
    {
        return tlLeagueMapper.updateTlLeague(tlLeague);
    }

    /**
     * 批量删除联赛
     * 
     * @param ids 需要删除的联赛主键
     * @return 结果
     */
    @Override
    public int deleteTlLeagueByIds(String ids)
    {
        return tlLeagueMapper.deleteTlLeagueByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除联赛信息
     * 
     * @param id 联赛主键
     * @return 结果
     */
    @Override
    public int deleteTlLeagueById(Long id)
    {
        return tlLeagueMapper.deleteTlLeagueById(id);
    }
}
