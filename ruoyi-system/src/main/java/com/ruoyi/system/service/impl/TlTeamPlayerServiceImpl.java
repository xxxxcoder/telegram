package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlTeamPlayerMapper;
import com.ruoyi.system.domain.TlTeamPlayer;
import com.ruoyi.system.service.ITlTeamPlayerService;
import com.ruoyi.common.core.text.Convert;

/**
 * 球员Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Service
public class TlTeamPlayerServiceImpl implements ITlTeamPlayerService 
{
    @Autowired
    private TlTeamPlayerMapper tlTeamPlayerMapper;

    /**
     * 查询球员
     * 
     * @param id 球员主键
     * @return 球员
     */
    @Override
    public TlTeamPlayer selectTlTeamPlayerById(Long id)
    {
        return tlTeamPlayerMapper.selectTlTeamPlayerById(id);
    }

    /**
     * 查询球员列表
     * 
     * @param tlTeamPlayer 球员
     * @return 球员
     */
    @Override
    public List<TlTeamPlayer> selectTlTeamPlayerList(TlTeamPlayer tlTeamPlayer)
    {
        return tlTeamPlayerMapper.selectTlTeamPlayerList(tlTeamPlayer);
    }

    /**
     * 新增球员
     * 
     * @param tlTeamPlayer 球员
     * @return 结果
     */
    @Override
    public int insertTlTeamPlayer(TlTeamPlayer tlTeamPlayer)
    {
        return tlTeamPlayerMapper.insertTlTeamPlayer(tlTeamPlayer);
    }

    /**
     * 修改球员
     * 
     * @param tlTeamPlayer 球员
     * @return 结果
     */
    @Override
    public int updateTlTeamPlayer(TlTeamPlayer tlTeamPlayer)
    {
        return tlTeamPlayerMapper.updateTlTeamPlayer(tlTeamPlayer);
    }

    /**
     * 批量删除球员
     * 
     * @param ids 需要删除的球员主键
     * @return 结果
     */
    @Override
    public int deleteTlTeamPlayerByIds(String ids)
    {
        return tlTeamPlayerMapper.deleteTlTeamPlayerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除球员信息
     * 
     * @param id 球员主键
     * @return 结果
     */
    @Override
    public int deleteTlTeamPlayerById(Long id)
    {
        return tlTeamPlayerMapper.deleteTlTeamPlayerById(id);
    }
}
