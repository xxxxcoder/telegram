package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlGameMapper;
import com.ruoyi.system.domain.TlGame;
import com.ruoyi.system.service.ITlGameService;
import com.ruoyi.common.core.text.Convert;

/**
 * 赛事赛程Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Service
public class TlGameServiceImpl implements ITlGameService 
{
    @Autowired
    private TlGameMapper tlGameMapper;

    /**
     * 查询赛事赛程
     * 
     * @param id 赛事赛程主键
     * @return 赛事赛程
     */
    @Override
    public TlGame selectTlGameById(Long id)
    {
        return tlGameMapper.selectTlGameById(id);
    }

    /**
     * 查询赛事赛程列表
     * 
     * @param tlGame 赛事赛程
     * @return 赛事赛程
     */
    @Override
    public List<TlGame> selectTlGameList(TlGame tlGame)
    {
        return tlGameMapper.selectTlGameList(tlGame);
    }

    /**
     * 新增赛事赛程
     * 
     * @param tlGame 赛事赛程
     * @return 结果
     */
    @Override
    public int insertTlGame(TlGame tlGame)
    {
        return tlGameMapper.insertTlGame(tlGame);
    }

    /**
     * 修改赛事赛程
     * 
     * @param tlGame 赛事赛程
     * @return 结果
     */
    @Override
    public int updateTlGame(TlGame tlGame)
    {
        return tlGameMapper.updateTlGame(tlGame);
    }

    /**
     * 批量删除赛事赛程
     * 
     * @param ids 需要删除的赛事赛程主键
     * @return 结果
     */
    @Override
    public int deleteTlGameByIds(String ids)
    {
        return tlGameMapper.deleteTlGameByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除赛事赛程信息
     * 
     * @param id 赛事赛程主键
     * @return 结果
     */
    @Override
    public int deleteTlGameById(Long id)
    {
        return tlGameMapper.deleteTlGameById(id);
    }
}
