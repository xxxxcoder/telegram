package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlTeamMapper;
import com.ruoyi.system.domain.TlTeam;
import com.ruoyi.system.service.ITlTeamService;
import com.ruoyi.common.core.text.Convert;

/**
 * 球队Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Service
public class TlTeamServiceImpl implements ITlTeamService 
{
    @Autowired
    private TlTeamMapper tlTeamMapper;

    /**
     * 查询球队
     * 
     * @param id 球队主键
     * @return 球队
     */
    @Override
    public TlTeam selectTlTeamById(Long id)
    {
        return tlTeamMapper.selectTlTeamById(id);
    }

    /**
     * 查询球队列表
     * 
     * @param tlTeam 球队
     * @return 球队
     */
    @Override
    public List<TlTeam> selectTlTeamList(TlTeam tlTeam)
    {
        return tlTeamMapper.selectTlTeamList(tlTeam);
    }

    /**
     * 新增球队
     * 
     * @param tlTeam 球队
     * @return 结果
     */
    @Override
    public int insertTlTeam(TlTeam tlTeam)
    {
        return tlTeamMapper.insertTlTeam(tlTeam);
    }

    /**
     * 修改球队
     * 
     * @param tlTeam 球队
     * @return 结果
     */
    @Override
    public int updateTlTeam(TlTeam tlTeam)
    {
        return tlTeamMapper.updateTlTeam(tlTeam);
    }

    /**
     * 批量删除球队
     * 
     * @param ids 需要删除的球队主键
     * @return 结果
     */
    @Override
    public int deleteTlTeamByIds(String ids)
    {
        return tlTeamMapper.deleteTlTeamByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除球队信息
     * 
     * @param id 球队主键
     * @return 结果
     */
    @Override
    public int deleteTlTeamById(Long id)
    {
        return tlTeamMapper.deleteTlTeamById(id);
    }
}
