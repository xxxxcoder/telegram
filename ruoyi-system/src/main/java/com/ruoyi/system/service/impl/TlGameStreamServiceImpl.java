package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlGameStreamMapper;
import com.ruoyi.system.domain.TlGameStream;
import com.ruoyi.system.service.ITlGameStreamService;
import com.ruoyi.common.core.text.Convert;

/**
 * 赛事线路Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
@Service
public class TlGameStreamServiceImpl implements ITlGameStreamService 
{
    @Autowired
    private TlGameStreamMapper tlGameStreamMapper;

    /**
     * 查询赛事线路
     * 
     * @param id 赛事线路主键
     * @return 赛事线路
     */
    @Override
    public TlGameStream selectTlGameStreamById(Long id)
    {
        return tlGameStreamMapper.selectTlGameStreamById(id);
    }

    /**
     * 查询赛事线路列表
     * 
     * @param tlGameStream 赛事线路
     * @return 赛事线路
     */
    @Override
    public List<TlGameStream> selectTlGameStreamList(TlGameStream tlGameStream)
    {
        return tlGameStreamMapper.selectTlGameStreamList(tlGameStream);
    }

    /**
     * 新增赛事线路
     * 
     * @param tlGameStream 赛事线路
     * @return 结果
     */
    @Override
    public int insertTlGameStream(TlGameStream tlGameStream)
    {
        return tlGameStreamMapper.insertTlGameStream(tlGameStream);
    }

    /**
     * 修改赛事线路
     * 
     * @param tlGameStream 赛事线路
     * @return 结果
     */
    @Override
    public int updateTlGameStream(TlGameStream tlGameStream)
    {
        return tlGameStreamMapper.updateTlGameStream(tlGameStream);
    }

    /**
     * 批量删除赛事线路
     * 
     * @param ids 需要删除的赛事线路主键
     * @return 结果
     */
    @Override
    public int deleteTlGameStreamByIds(String ids)
    {
        return tlGameStreamMapper.deleteTlGameStreamByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除赛事线路信息
     * 
     * @param id 赛事线路主键
     * @return 结果
     */
    @Override
    public int deleteTlGameStreamById(Long id)
    {
        return tlGameStreamMapper.deleteTlGameStreamById(id);
    }
}
