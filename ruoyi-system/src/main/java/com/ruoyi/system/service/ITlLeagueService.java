package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TlLeague;

/**
 * 联赛Service接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface ITlLeagueService 
{
    /**
     * 查询联赛
     * 
     * @param id 联赛主键
     * @return 联赛
     */
    public TlLeague selectTlLeagueById(Long id);

    /**
     * 查询联赛列表
     * 
     * @param tlLeague 联赛
     * @return 联赛集合
     */
    public List<TlLeague> selectTlLeagueList(TlLeague tlLeague);

    /**
     * 新增联赛
     * 
     * @param tlLeague 联赛
     * @return 结果
     */
    public int insertTlLeague(TlLeague tlLeague);

    /**
     * 修改联赛
     * 
     * @param tlLeague 联赛
     * @return 结果
     */
    public int updateTlLeague(TlLeague tlLeague);

    /**
     * 批量删除联赛
     * 
     * @param ids 需要删除的联赛主键集合
     * @return 结果
     */
    public int deleteTlLeagueByIds(String ids);

    /**
     * 删除联赛信息
     * 
     * @param id 联赛主键
     * @return 结果
     */
    public int deleteTlLeagueById(Long id);
}
