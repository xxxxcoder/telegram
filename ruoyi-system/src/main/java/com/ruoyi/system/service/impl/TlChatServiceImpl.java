package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.common.utils.stream.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TlChatMapper;
import com.ruoyi.system.domain.TlChat;
import com.ruoyi.system.service.ITlChatService;
import com.ruoyi.common.core.text.Convert;

/**
 * 电报-会话Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
@Service
public class TlChatServiceImpl implements ITlChatService 
{
    private static final Logger log = LoggerFactory.getLogger(TlChatServiceImpl.class);
    @Autowired
    private TlChatMapper tlChatMapper;

    /**
     * 查询电报-会话
     * 
     * @param id 电报-会话主键
     * @return 电报-会话
     */
    @Override
    public TlChat selectTlChatById(Long id)
    {
        return tlChatMapper.selectTlChatById(id);
    }

    /**
     * 查询电报-会话列表
     * 
     * @param tlChat 电报-会话
     * @return 电报-会话
     */
    @Override
    public List<TlChat> selectTlChatList(TlChat tlChat)
    {
        return tlChatMapper.selectTlChatList(tlChat);
    }

    /**
     * 新增电报-会话
     * 
     * @param tlChat 电报-会话
     * @return 结果
     */
    @Override
    public int insertTlChat(TlChat tlChat)
    {
        return tlChatMapper.insertTlChat(tlChat);
    }

    /**
     * 修改电报-会话
     * 
     * @param tlChat 电报-会话
     * @return 结果
     */
    @Override
    public int updateTlChat(TlChat tlChat)
    {
        return tlChatMapper.updateTlChat(tlChat);
    }

    /**
     * 批量删除电报-会话
     * 
     * @param ids 需要删除的电报-会话主键
     * @return 结果
     */
    @Override
    public int deleteTlChatByIds(String ids)
    {
        return tlChatMapper.deleteTlChatByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除电报-会话信息
     * 
     * @param id 电报-会话主键
     * @return 结果
     */
    @Override
    public int deleteTlChatById(Long id)
    {
        return tlChatMapper.deleteTlChatById(id);
    }

    @Override
    public void autoPushStream() {
        TlChatMapper chatMapper = SpringUtils.getBean(TlChatMapper.class);
        List<TlChat> chats = chatMapper.selectTlChatList(new TlChat());
        for (TlChat chat : chats) {
            log.info("定时自动转推流任务，{}",chat.getChatName());
            boolean check = StreamUtils.checkStream(Long.valueOf(chat.getChatId()));
            if(!check){
                Long tid = StreamUtils.pushStream(chat.getStreamSrc(), chat.getStreamUrl());
                chat.setChatId(String.valueOf(tid));
                chatMapper.updateTlChat(chat);
            }
        }
    }


}
