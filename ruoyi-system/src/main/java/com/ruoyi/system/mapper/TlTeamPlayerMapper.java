package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TlTeamPlayer;

/**
 * 球员Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface TlTeamPlayerMapper 
{
    /**
     * 查询球员
     * 
     * @param id 球员主键
     * @return 球员
     */
    public TlTeamPlayer selectTlTeamPlayerById(Long id);

    /**
     * 查询球员列表
     * 
     * @param tlTeamPlayer 球员
     * @return 球员集合
     */
    public List<TlTeamPlayer> selectTlTeamPlayerList(TlTeamPlayer tlTeamPlayer);

    /**
     * 新增球员
     * 
     * @param tlTeamPlayer 球员
     * @return 结果
     */
    public int insertTlTeamPlayer(TlTeamPlayer tlTeamPlayer);

    /**
     * 修改球员
     * 
     * @param tlTeamPlayer 球员
     * @return 结果
     */
    public int updateTlTeamPlayer(TlTeamPlayer tlTeamPlayer);

    /**
     * 删除球员
     * 
     * @param id 球员主键
     * @return 结果
     */
    public int deleteTlTeamPlayerById(Long id);

    /**
     * 批量删除球员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTlTeamPlayerByIds(String[] ids);
}
