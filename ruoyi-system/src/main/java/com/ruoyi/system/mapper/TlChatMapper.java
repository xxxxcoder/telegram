package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TlChat;

/**
 * 会话群组Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface TlChatMapper 
{
    /**
     * 查询会话群组
     * 
     * @param id 会话群组主键
     * @return 会话群组
     */
    public TlChat selectTlChatById(Long id);

    /**
     * 查询会话群组列表
     * 
     * @param tlChat 会话群组
     * @return 会话群组集合
     */
    public List<TlChat> selectTlChatList(TlChat tlChat);

    /**
     * 新增会话群组
     * 
     * @param tlChat 会话群组
     * @return 结果
     */
    public int insertTlChat(TlChat tlChat);

    /**
     * 修改会话群组
     * 
     * @param tlChat 会话群组
     * @return 结果
     */
    public int updateTlChat(TlChat tlChat);

    /**
     * 删除会话群组
     * 
     * @param id 会话群组主键
     * @return 结果
     */
    public int deleteTlChatById(Long id);

    /**
     * 批量删除会话群组
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTlChatByIds(String[] ids);
}
