package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TlTeam;

/**
 * 球队Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface TlTeamMapper 
{
    /**
     * 查询球队
     * 
     * @param id 球队主键
     * @return 球队
     */
    public TlTeam selectTlTeamById(Long id);

    /**
     * 查询球队列表
     * 
     * @param tlTeam 球队
     * @return 球队集合
     */
    public List<TlTeam> selectTlTeamList(TlTeam tlTeam);

    /**
     * 新增球队
     * 
     * @param tlTeam 球队
     * @return 结果
     */
    public int insertTlTeam(TlTeam tlTeam);

    /**
     * 修改球队
     * 
     * @param tlTeam 球队
     * @return 结果
     */
    public int updateTlTeam(TlTeam tlTeam);

    /**
     * 删除球队
     * 
     * @param id 球队主键
     * @return 结果
     */
    public int deleteTlTeamById(Long id);

    /**
     * 批量删除球队
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTlTeamByIds(String[] ids);
}
