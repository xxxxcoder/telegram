package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TlGameStream;

/**
 * 赛事线路Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-23
 */
public interface TlGameStreamMapper 
{
    /**
     * 查询赛事线路
     * 
     * @param id 赛事线路主键
     * @return 赛事线路
     */
    public TlGameStream selectTlGameStreamById(Long id);

    /**
     * 查询赛事线路列表
     * 
     * @param tlGameStream 赛事线路
     * @return 赛事线路集合
     */
    public List<TlGameStream> selectTlGameStreamList(TlGameStream tlGameStream);

    /**
     * 新增赛事线路
     * 
     * @param tlGameStream 赛事线路
     * @return 结果
     */
    public int insertTlGameStream(TlGameStream tlGameStream);

    /**
     * 修改赛事线路
     * 
     * @param tlGameStream 赛事线路
     * @return 结果
     */
    public int updateTlGameStream(TlGameStream tlGameStream);

    /**
     * 删除赛事线路
     * 
     * @param id 赛事线路主键
     * @return 结果
     */
    public int deleteTlGameStreamById(Long id);

    /**
     * 批量删除赛事线路
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTlGameStreamByIds(String[] ids);
}
